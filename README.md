<h1 align="center"> weather </h1>

<p align="center"> Laravel 5.8 Weather.</p>


## 安装

```shell
$ composer require end01here/weather -vvv
```

## 使用

```php
use End01here\Weather\Weather;

$key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxx';

$weather = new Weather($key);

```



## 获取实时天气

```php
$response = $weather->getWeather('深圳');
```
返回示列
```json

{
  "status": "1",
  "count": "1",
  "info": "OK",
  "infocode": "10000",
  "lives": [
    {
      "province": "重庆",
      "city": "重庆市",
      "adcode": "500000",
      "weather": "晴",
      "temperature": "23",
      "winddirection": "东北",
      "windpower": "≤3",
      "humidity": "39",
      "reporttime": "2020-11-12 15:26:02"
    }
  ]
}

```

## 获取近期天气

```php
$response = $weather->getWeather('深圳','all');
```
返回示列
```json

{
  "status": "1",
  "count": "1",
  "info": "OK",
  "infocode": "10000",
  "forecasts": [
    {
      "city": "重庆市",
      "adcode": "500000",
      "province": "重庆",
      "reporttime": "2020-11-12 15:26:02",
      "casts": [
        {
          "date": "2020-11-12",
          "week": "4",
          "dayweather": "晴",
          "nightweather": "晴",
          "daytemp": "24",
          "nighttemp": "12",
          "daywind": "东南",
          "nightwind": "东南",
          "daypower": "≤3",
          "nightpower": "≤3"
        },
        {
          "date": "2020-11-13",
          "week": "5",
          "dayweather": "晴",
          "nightweather": "晴",
          "daytemp": "24",
          "nighttemp": "14",
          "daywind": "东北",
          "nightwind": "东北",
          "daypower": "≤3",
          "nightpower": "≤3"
        },
        {
          "date": "2020-11-14",
          "week": "6",
          "dayweather": "晴",
          "nightweather": "晴",
          "daytemp": "25",
          "nighttemp": "14",
          "daywind": "东北",
          "nightwind": "东北",
          "daypower": "≤3",
          "nightpower": "≤3"
        },
        {
          "date": "2020-11-15",
          "week": "7",
          "dayweather": "晴",
          "nightweather": "晴",
          "daytemp": "21",
          "nighttemp": "12",
          "daywind": "东北",
          "nightwind": "东北",
          "daypower": "≤3",
          "nightpower": "≤3"
        }
      ]
    }
  ]
}

```

## 获取 XML 格式返回值
   第三个参数为返回值类型，可选 json 与 xml，默认 json：

## 参数说明
```php
array|string getWeather(string $city, string $type = 'base', string $format = 'json')
```
$city - 城市名，比如：“深圳”；
$type - 返回内容类型：base: 返回实况天气 / all: 返回预报天气；
$format  - 输出的数据格式，默认为 json 格式，当 output 设置为 “xml” 时，输出的为 XML 格式的数据。

##在 Laravel 中使用
使用命令,复制配置文件至 config/weather_services.php
```shell 
$ php artisan vendor:publish --provider="End01here\
Weather\ServiceProvider"
```
可以再 .env 或者 config/weather_services.php 文件中修改高德API的key

