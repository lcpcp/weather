<?php


namespace End01here\Weather;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * 设置框架的延迟加载
     * @var bool
     */
    protected $defer = true;

    /**
     * 将扩展注册到框架中
     */
    public function register()
    {
        $configPath = __DIR__ . '/../config/weather_services.php';
        $this->mergeConfigFrom($configPath, 'weather_services');
        $this->app->singleton(Weather::class, function(){
            return new Weather(config('weather_services.weather.key'));
        });

        $this->app->alias(Weather::class, 'weather_services');
    }

    public function provides()
    {
        return [Weather::class, 'weather'];
    }

    /**
     * Bootstrap the application events.
     *注册组件。将项目的配置文件复制至系统配置文件
     * @return void
     */
    public function boot()
    {

        $configPath = __DIR__ . '/../config/weather_services.php';
        $this->publishes([$configPath => config_path('weather_services.php')], 'weather_services');

    }




}