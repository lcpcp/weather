<?php


namespace End01here\Weather\Tests;


use End01here\Weather\Exceptions\HttpException;
use End01here\Weather\Exceptions\InvalidArgumentException;
use End01here\Weather\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Mockery\Matcher\AnyArgs;
use PHPUnit\Framework\TestCase;

class WeatherTest extends TestCase
{
    public $test_key = '';

    function testGetWeather()
    {
        //设置网络请求的返回对象格式 响应code 200 响应头信息空 返回信息为json数据{"success": true}
        $response = new Response(200, [], '{"success": true}');
        //设置一个Client的伪装对象
        $client = \Mockery::mock(Client::class);
        //设置Client对象在请求下面的地址和对应参数的时候返回上面生成的对应参数
        $client->allows()->get('https://restapi.amap.com/v3/weather/weatherInfo', [
            'query' => [
                'key' => 'mock-key',
                'city' => '深圳',
                'output' => 'json',
                'extensions' => 'base',
            ],
        ])->andReturn($response);

        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client);

        $this->assertSame(['success' => true], $w->getWeather('深圳'));


        // xml 原理与json原理一样
        $response = new Response(200, [], '<hello>content</hello>');
        $client = \Mockery::mock(Client::class);
        $client->allows()->get('https://restapi.amap.com/v3/weather/weatherInfo', [
            'query' => [
                'key' => 'mock-key',
                'city' => '深圳',
                'extensions' => 'all',
                'output' => 'xml',
            ],
        ])->andReturn($response);

        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client);

        $this->assertSame('<hello>content</hello>', $w->getWeather('深圳', 'all', 'xml'));
    }

    /**
     * 测试在http请求超时的错误检查
     */
    function testGetWeatherWithGuzzleRuntimeException()
    {
        //设置一个Client的伪装对象
        $client = \Mockery::mock(Client::class);
        //设置伪装对象执行get参数的时候，抛出一个系统错误，错误内容为 request timeout
        $client->allows()
            ->get(new AnyArgs())
            ->andThrow(new \Exception('request timeout'));

        //创建一个Weather伪装对象，并且将Weather对象的key这个参数复制过来，makePartial()这个方法是在遇见错误的时候使用伪装对象里面的报错机制
        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client);

        //断言系统会抛出一个HttpException错误
        $this->expectException(HttpException::class);
        //断言系统返回的错误信息是'request timeout'
        $this->expectExceptionMessage('request timeout');
        //执行请求
        $w->getWeather('深圳');
        $this->fail('系统错误抛出错误.');
    }


    /**
     * 单元测试Weather 类中的testGetHttpClient 方法是否返回对应Client对象
     */
    function testGetHttpClient()
    {
        $w = new Weather($this->test_key);
        //判断Weather对象的getHttpClient方法返回的是否是Client对象
        $this->assertInstanceOf(Client::class, $w->getHttpClient());
    }

    /**
     * 测试设置参数功能是否正确
     */
    function testSetGuzzleOptions()
    {
        $w = new Weather($this->test_key);
        //设置Client对象的timeout初始化为null
        $this->assertNull($w->getHttpClient()->getConfig('timeout'));
        //设置Client对象的timeout参数为5000
        $w->setGuzzleOptions(['timeout' => 5000]);
        //验证Client对象的timeout是否是5000，是则测试通过，不是则测试错误
        $this->assertSame(5000, $w->getHttpClient()->getConfig('timeout'));
    }

    public function testGetWeatherWithInvalidType()
    {
        $w = new Weather($this->test_key);

        // 断言会抛出此异常类
        $this->expectException(InvalidArgumentException::class);

        // 断言异常消息为 'Invalid type value(base/all): foo'
        $this->expectExceptionMessage('Invalid type value(base/all): foo');

        $w->getWeather('深圳', 'foo');

        $this->fail('Failed to assert getWeather throw exception with invalid argument.');
    }

    // 检查 $format 参数
    public function testGetWeatherWithInvalidFormat()
    {
        $w = new Weather($this->test_key);

        // 断言会抛出此异常类
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage('Invalid type format(xml/json): array');

        $w->getWeather('深圳', 'base', 'array');

        $this->fail('Failed to assert getWeather throw exception with invalid argument.');
    }


}